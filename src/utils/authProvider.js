import decodeJwt from 'jwt-decode';

const apiUrl = process.env.REACT_APP_API_PATH

const authProvider = {
  login: ({ username, password}) => {
    const request = new Request(`${apiUrl}/auth`, {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: new Headers({ 'Content-Type': 'application/json' })
    })
    return fetch(request)
      .then(response => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error(response.statusText)
        }
        return response.json()
      })
      .then(({ jwt: token }) => {
        const decodedToken = decodeJwt(token)
        localStorage.setItem('token', token)
        localStorage.setItem('permissions', decodedToken.sub) // sub - user's role WIP
      })
  },
  logout: () => {
    localStorage.removeItem('token')
    localStorage.removeItem('permissions')
    return Promise.resolve()
  },
  checkError: (error) => {
    const status = error.status;
    if (status === 401 || status === 403) {
      localStorage.removeItem('token');
      return Promise.reject();
    }
    return Promise.resolve();
  },
  checkAuth: () => {
    return localStorage.getItem('token') ? Promise.resolve() : Promise.reject();
  },
  getPermissions: () => {
    const role = localStorage.getItem('permissions')
    return role ? Promise.resolve(role): Promise.reject()
  }
}

export default authProvider
